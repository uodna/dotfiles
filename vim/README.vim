# Requirements

* Python3

```
env PYTHON_CONFIGURE_OPTS="--enable-framework" pyenv install 3.6.3
pyenv virtualenv neovim3 3.6.3
pyenv activate neovim3
pip install neovim
```

* Boost

```
brew install boost
```

